package com.developex.websearcher;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.developex.websearcher.data.Config;
import com.developex.websearcher.data.PageNode;
import com.developex.websearcher.data.ResultItem;
import com.developex.websearcher.data.ResultState;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class SearchActivity extends Activity {
    private static final String LOG_TAG = SearchActivity.class.getSimpleName();
    private static final String DEFAULT_URL = "http://bash.im";

    @InjectView(R.id.et_threads_count)
    EditText etThreadsCount;
    @InjectView(R.id.et_urls_count)
    EditText etUrlsCount;
    @InjectView(R.id.et_start_url)
    EditText etStartUrl;
    @InjectView(R.id.et_search_text)
    EditText etSearchText;
    @InjectView(R.id.lv_search_results)
    ListView lvResults;

    private static Config config;
    private static AtomicInteger currentUrlsCount = new AtomicInteger(0);
    private static AtomicInteger currentThreadsCount = new AtomicInteger(0);
    private Queue<PageNode> currentLevel = new LinkedList<PageNode>();
    private static final Queue<Thread> taskQueue = new LinkedList<Thread>();
    private SearchResultAdapter resultsAdapter;
    private Set<String> visited = new HashSet<String>();
    private boolean stopFlag;
    private MenuItem restartMenuitem;
    private MenuItem startMenuItem;
    private MenuItem stopMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ButterKnife.inject(this);
        setupViews();
    }

    private void setupViews() {
        etSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyboard(v.getWindowToken());
                    restartSearch();
                    enableMenuItems(true);
                    return true;
                }

                return false;
            }
        });

        lvResults.setAdapter(getEmptyAdapter());
    }

    private void hideKeyboard(IBinder token) {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);

        imm.hideSoftInputFromWindow(token, 0);
    }

    private SearchResultAdapter getEmptyAdapter() {
        return resultsAdapter = new SearchResultAdapter(this);
    }

    private void stopSearch() {
        stopFlag = true;
        resultsAdapter.changeState(ResultState.DOWNLOADING, ResultState.NOT_FOUND);
    }

    private void restartSearch() {
        stopFlag = false;
        clearParams();
        startSearch();
    }

    private void startSearch() {
        config = buildConfig();
        if (config == null) {
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                currentLevel.add(PageNode.getBuilder()
                        .setNodeUrl(config.getStartUrl())
                        .build());

                while (currentUrlsCount.get() < config.getMaxUrlCount() && !stopFlag) {
                    if (currentThreadsCount.get() < config.getMaxThreadCount()) {
                        PageNode node = currentLevel.poll();

                        if (node == null || node.getNodeUrl().trim().isEmpty()) {
                            continue;
                        }

                        currentThreadsCount.incrementAndGet();
                        currentUrlsCount.incrementAndGet();

                        addDownloadingItem(node);
                        visited.add(node.getNodeUrl().trim());
                        startDownloadTask(node);
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                taskQueue.clear();
            }
        }.execute();
    }

    private void startDownloadTask(final PageNode node) {
        new DownloadThread(node).start();
    }

    private List<PageNode> elementsToList(Elements links) {
        Set<PageNode> childs = new HashSet<PageNode>(links.size());

        for (Element link : links) {
            String url = link.attr("abs:href");

            if (visited.contains(url.trim())) {
                continue;
            }

            visited.add(url);
            childs.add(PageNode.getBuilder()
                    .setNodeUrl(url)
                    .build());
        }

        return new ArrayList<PageNode>(childs);
    }

    private void addItemToAdapter(final ResultItem item) {
        if (stopFlag || currentUrlsCount.get() >= config.getMaxUrlCount()) {
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (item.getState() == ResultState.DOWNLOADING) {
                    resultsAdapter.add(item);
                } else {
                    resultsAdapter.updateItem(item);
                }
            }
        });
    }

    private void addDownloadingItem(PageNode node) {
        addItemToAdapter(getDownloadingItem(node));
    }

    private ResultItem.ResultItemBuilder getDefaultItemBuilder(PageNode node, ResultState state) {
        return ResultItem.getBuilder()
                .setPageUrl(node.getNodeUrl())
                .setState(state)
                .setMsg("");
    }

    // should be done with factory but not today..
    private ResultItem getErrorItem(PageNode node, String msg) {
        return  getDefaultItemBuilder(node, ResultState.ERROR).setMsg(msg).build();
    }

    private ResultItem getFoundItem(PageNode node) {
        return getDefaultItemBuilder(node, ResultState.FOUND).build();
    }

    private ResultItem getNotFoundItem(PageNode node) {
        return getDefaultItemBuilder(node, ResultState.NOT_FOUND).build();
    }

    private ResultItem getDownloadingItem(PageNode node) {
        return getDefaultItemBuilder(node, ResultState.DOWNLOADING).build();
    }

    private void clearParams() {
        while (!taskQueue.isEmpty()) {
            Thread thread = taskQueue.poll();
            thread.interrupt();
        }

        currentUrlsCount.set(0);
        currentThreadsCount.set(0);

        currentLevel.clear();
        resultsAdapter.clear();
        visited.clear();
    }

    private Config buildConfig() {
        String threadsCount = etThreadsCount.getText().toString();
        String urlsCount = etUrlsCount.getText().toString();
        String starturl = etStartUrl.getText().toString();
        String searchText = etSearchText.getText().toString();

        if (threadsCount.trim().isEmpty() || Integer.parseInt(threadsCount.trim().toString()) <= 0) {
            showErrToast(R.string.threads_count_zero_err);
            return null;
        } else if (urlsCount.trim().isEmpty() || Integer.parseInt(urlsCount.trim().toString()) <= 0) {
            showErrToast(R.string.url_count_zero_err);
            return null;
        } else if (starturl.trim().isEmpty()) {
            showErrToast(R.string.start_url_empty_err);
            return null;
        } else if (searchText.trim().isEmpty()) {
            showErrToast(R.string.search_text_empty_err);
            return null;
        }

        return Config.getBuilder()
            .setMaxThreadCount(Integer.parseInt(etThreadsCount.getText().toString()))
            .setMaxUrlCount(Integer.parseInt(etUrlsCount.getText().toString()))
            .setStartUrl(etStartUrl.getText().toString())
            .setSearchText(etSearchText.getText().toString())
            .build();
    }

    private void showErrToast(int stringId) {
        Toast.makeText(this, stringId, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        getMenuInflater().inflate(R.menu.main_menu, menu);

        restartMenuitem = menu.findItem(R.id.restart_menu_item);
        startMenuItem = menu.findItem(R.id.start_menu_item);
        stopMenuItem = menu.findItem(R.id.stop_menu_item);

        enableMenuItems(false);
        return true;
    }

    private void enableMenuItems(boolean isEnabled) {
        if (restartMenuitem != null) {
            restartMenuitem.setEnabled(isEnabled);
        }
        if (startMenuItem != null) {
            startMenuItem.setEnabled(isEnabled);
        }
        if (stopMenuItem != null) {
            stopMenuItem.setEnabled(isEnabled);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        hideKeyboard(etSearchText.getWindowToken());
        switch (item.getItemId()) {
            case R.id.restart_menu_item:
                restartSearch();
                break;
            case R.id.start_menu_item:
                restartSearch();
                break;
            case R.id.stop_menu_item:
                stopSearch();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private class DownloadThread extends Thread {
        private PageNode node;

        public DownloadThread(PageNode node) {
            this.node = node;
        }

        @Override
        public void run() {
            doJob();
        }

        @Override
        public void interrupt() {
            super.interrupt();
            doPostJob();
        }

        private void doPreJob() {
            taskQueue.add(this);
        }

        private void doJob() {
            doPreJob();
            Document doc;
            try {
                doc = Jsoup.connect(node.getNodeUrl()).timeout(0).get();
            } catch (IOException e) {
                addItemToAdapter(getErrorItem(node, Log.getStackTraceString(e)));
                return;
            }

            if (doc == null) {
                Log.e(LOG_TAG, "doc == null so return null");
                return;
            }

            findTextInDoc(doc);

            Elements links = doc.select("a[href]");
            List<PageNode> childs = elementsToList(links);

            currentLevel.addAll(childs);
            doPostJob();
        }

        private void findTextInDoc(Document doc) {
            String docText = doc.body().text();
            if (docText.toLowerCase().contains(config.getSearchText().toLowerCase())) {
                addItemToAdapter(getFoundItem(node));
            } else {
                addItemToAdapter(getNotFoundItem(node));
            }
        }

        private void doPostJob() {
            currentThreadsCount.decrementAndGet();
            taskQueue.remove(this);
        }
    }
}