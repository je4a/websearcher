package com.developex.websearcher;

import android.util.Log;

import java.lang.reflect.ParameterizedType;

public abstract class BaseBuilder<T> {
    private static final String LOG_TAG = BaseBuilder.class.getSimpleName();
    protected T data;

    public BaseBuilder() {
        try {
            data = getTypeParameterClass().newInstance();
        } catch (InstantiationException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
        } catch (IllegalAccessException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
        }
    }

    public T build() {
        return data;
    }

    private Class<T> getTypeParameterClass()
    {
        ParameterizedType paramType = (ParameterizedType) getClass().getGenericSuperclass();
        return (Class<T>) paramType.getActualTypeArguments()[0];
    }
}