package com.developex.websearcher.data;

import com.developex.websearcher.BaseBuilder;

public class ResultItem {
    private String pageUrl;
    private ResultState state = ResultState.DOWNLOADING;
    private String msg;

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public ResultState getState() {
        return state;
    }

    public void setState(ResultState state) {
        this.state = state;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static ResultItemBuilder getBuilder() {
        return new ResultItemBuilder();
    }

    public static class ResultItemBuilder extends BaseBuilder<ResultItem> {

        ResultItemBuilder() {
            super();
        }

        public ResultItemBuilder setPageUrl(String pageUrl) {
            data.setPageUrl(pageUrl);
            return this;
        }

        public ResultItemBuilder setState(ResultState state) {
            data.setState(state);
            return this;
        }

        public ResultItemBuilder setMsg(String msg) {
            data.setMsg(msg);
            return this;
        }
    }
}