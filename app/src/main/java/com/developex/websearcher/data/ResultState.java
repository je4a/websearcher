package com.developex.websearcher.data;

public enum ResultState {
    DOWNLOADING(android.R.drawable.stat_sys_download),
    FOUND(android.R.drawable.presence_online),
    NOT_FOUND(android.R.drawable.ic_delete),
    ERROR(android.R.drawable.stat_notify_error);

    private int drawableId;

    private ResultState(int drawableId) {
        this.drawableId = drawableId;
    }

    public int getDrawableId() {
        return drawableId;
    }
}
