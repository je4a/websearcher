package com.developex.websearcher.data;

import com.developex.websearcher.BaseBuilder;

import java.util.ArrayList;
import java.util.List;

public class PageNode {
    private String nodeUrl;
    private List<PageNode> childs = new ArrayList<PageNode>();

    public String getNodeUrl() {
        return nodeUrl;
    }

    public void setNodeUrl(String nodeUrl) {
        this.nodeUrl = nodeUrl;
    }

    public List<PageNode> getChilds() {
        return childs;
    }

    public void setChilds(List<PageNode> childs) {
        this.childs = childs;
    }

    public static PageNodeBuilder getBuilder() {
        return new PageNodeBuilder();
    }

    public static class PageNodeBuilder extends BaseBuilder<PageNode> {

        PageNodeBuilder() {
            super();
        }

        public PageNodeBuilder setNodeUrl(String nodeUrl) {
            data.setNodeUrl(nodeUrl);
            return this;
        }

        public PageNodeBuilder setChilds(List<PageNode> childs) {
            data.setChilds(childs);
            return this;
        }
    }
}