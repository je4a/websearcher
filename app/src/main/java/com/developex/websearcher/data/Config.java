package com.developex.websearcher.data;

import com.developex.websearcher.BaseBuilder;

public class Config {
    private int maxThreadCount;
    private int maxUrlCount;
    private String startUrl;
    private String searchText;

    public int getMaxThreadCount() {
        return maxThreadCount;
    }

    public void setMaxThreadCount(int maxThreadCount) {
        this.maxThreadCount = maxThreadCount;
    }

    public int getMaxUrlCount() {
        return maxUrlCount;
    }

    public void setMaxUrlCount(int maxUrlCount) {
        this.maxUrlCount = maxUrlCount;
    }

    public String getStartUrl() {
        return startUrl;
    }

    public void setStartUrl(String startUrl) {
        this.startUrl = startUrl;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    @Override
    public String toString() {
        return maxThreadCount + " " + maxUrlCount + " " + startUrl + " " + searchText;
    }

    public static ConfigBuilder getBuilder() {
        return new ConfigBuilder();
    }

    public static class ConfigBuilder extends BaseBuilder<Config> {

        ConfigBuilder() {
            super();
        }

        public ConfigBuilder setMaxThreadCount(int maxThreadCount) {
            data.setMaxThreadCount(maxThreadCount);
            return this;
        }

        public ConfigBuilder setMaxUrlCount(int maxUrlCount) {
            data.setMaxUrlCount(maxUrlCount);
            return this;
        }

        public ConfigBuilder setStartUrl(String startUrl) {
            data.setStartUrl(startUrl);
            return this;
        }

        public ConfigBuilder setSearchText(String searchText) {
            data.setSearchText(searchText);
            return this;
        }
    }
}
