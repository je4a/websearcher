package com.developex.websearcher;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.developex.websearcher.data.ResultItem;
import com.developex.websearcher.data.ResultState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class SearchResultAdapter extends BaseAdapter {
    private static Resources resources;
    private LayoutInflater inflater;
    private Map<String, Integer> positions = new HashMap<String, Integer>();
    private List<ResultItem> items = new ArrayList<ResultItem>(30);

    public SearchResultAdapter(Context context) {
        resources = context.getResources();
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public ResultItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(ResultItem item) {
        items.add(item);
        positions.put(item.getPageUrl(), getCount() - 1);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.search_result_list_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final ResultItem item = getItem(position);
        holder.setItem(item);
        return convertView;
    }

    public static class ViewHolder {
        @InjectView(R.id.tv_url)
        public TextView tvUrl;
        @InjectView(R.id.tv_status)
        public TextView tvStatus;
        @InjectView(R.id.pbProgress)
        ProgressBar progressBar;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

        public void setItem(ResultItem item) {
            tvUrl.setText(item.getPageUrl());
            tvStatus.setText(item.getMsg());
            progressBar.setVisibility(item.getState() != ResultState.DOWNLOADING ? View.GONE : View.VISIBLE);

            if (item.getState() == ResultState.ERROR) {
                tvStatus.setCompoundDrawablesWithIntrinsicBounds(0, item.getState().getDrawableId(), 0, 0);
            } else {
                tvStatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, item.getState().getDrawableId(), 0);
            }
        }
    }

    public void updateItem(ResultItem item) {
        Integer position = positions.get(item.getPageUrl());

        if (position == null) {
            return;
        }

        ResultItem dataItem = getItem(position);

        dataItem.setState(item.getState());
        dataItem.setMsg(item.getMsg());
        notifyDataSetChanged();
    }

    public void changeState(ResultState from, ResultState to) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getState() == from) {
                items.get(i).setState(to);
            }
        }

        notifyDataSetChanged();
    }

    public void clear() {
        items.clear();
        positions.clear();
        notifyDataSetChanged();
    }
}
